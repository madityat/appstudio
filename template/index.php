<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themeum.com/html/multi/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 01 Oct 2017 04:31:42 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Appstudio | Web & Mobile Development</title>
    <!-- core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/owl.carousel.css" rel="stylesheet">
    <link href="../css/owl.transitions.css" rel="stylesheet">
    <link href="../css/prettyPhoto.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="../images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/apple-touch-icon-57-precomposed.png">

    
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyBw4UXt34osBTaVCkxURHMbwxZU2eRXjHA&callback=initMap"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/mousescroll.js"></script>
    <script src="../js/smoothscroll.js"></script>
    <script src="../js/jquery.prettyPhoto.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/jquery.inview.min.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/main.js"></script>
</head>
<!--/head-->
<body class="homepage">
    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../index.php"><img src="../images/logo.png" alt="logo"></a>
                </div>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="scroll active"><a href="../#home">Home</a></li>
                        <li class="scroll"><a href="../#services">Services</a></li>
                        <li class="scroll"><a href="../#portfolio">Portfolio</a></li>
                        <li class="scroll"><a href="../#meet-team">Team</a></li>
                        <li class="scroll"><a href="../#pricing">Pricing</a></li>
                        <li class="scroll"><a href="../#about">About</a></li>
                        <li class="scroll"><a href="#">Website Template</a></li>
                    </ul>
                </div>
            </div>
            <!--/.container-->
        </nav>
        <!--/nav-->
    </header>
    <!--/header-->

    <section id="portfolio">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Template List</h2>
                <p class="text-center wow fadeInDown">Tentukan sendiri design dan template yang kamu inginkan.</p>
            </div>
            <div class="text-center">
                <ul class="portfolio-filter">
                    <li><a class="active" href="#" data-filter="*">All Design</a></li>
                </ul>
                <!--/#portfolio-filter-->
            </div>
            <div class="portfolio-items">
            <?php
                $no = 0;
                foreach (glob("*") as $filename) {
                    if (substr($filename, -4) === ".php") {
                        continue;
                    }
                    //website url
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $siteURL = urlencode($actual_link.$filename);
                    echo "<input type='hidden' id='site[".$no."]' value='".$siteURL."'>";
                    ?>
                        <div class="portfolio-item design">
                            <div class="portfolio-item-inner">
                                <img id="imageSite[<?php echo $no; ?>]" class="img-responsive" src="../images/preview/preview.gif" alt="">
                                <div class="portfolio-info">
                                    <h3><?php echo strtoupper($filename); ?></h3>
                                    <a class="preview" href="<?php echo "./$filename"?>" target="_blank"><i class="fa fa-eye"></i></a>
                                </div>
                            </div>
                        </div>
                        <script>
                            $.get('./getWebPreview.php?url=<?php echo $siteURL; ?>', function(data, status){
                                if (status === 'success') {
                                    document.getElementById('imageSite[<?php echo $no; ?>]').src = data;
                                }
                            });
                        </script>
                        <!--/.portfolio-item-->
                    <?php
                    $no++;
                }
                echo "<input type='hidden' id='countSite' value='".$no."'>";
            ?>
            </div>
        </div>
        <!--/.container-->
    </section>
    <!--/#portfolio-->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2017 Appstudio.id. Powered by <a target="_blank" href="https://shapebootstrap.net/">ShapeBootstrap</a>.
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script>
        // window.addEventListener('load', function () {
        //     var countSite = document.getElementById('countSite').value;
        //     for (var i=0; i<countSite ; i++) {
        //         var site = document.getElementById('site['+i+']').value;
        //         $.get("./getWebPreview.php?url="+site, function(data, status){
        //             if (status === 'success') {
        //                 var imageSrc = document.getElementById('imageSite['+i+']').src;
        //                 alert(imageSrc);
        //             }
        //         });
        //     }
        // });
    </script>
</body>


<!-- Mirrored from demo.themeum.com/html/multi/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 01 Oct 2017 04:32:45 GMT -->
</html>